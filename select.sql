--- select文作成にあたり練習がてら作成 ---


-- 性別ごとの購入数の抽出SQL
select gender_id AS '性別' from customer group by gender_id having gender_id;

-- customerテーブルとgenderテーブルを結合
SELECT customer.id AS '会員ID',customer.name AS '氏名',customer.barthday AS '生年月日',gender.name AS '性別',customer.create_at AS '作成日',customer.update_at AS '更新日' 
FROM customer 
LEFT OUTER JOIN gender 
ON customer.gender_id = gender.id
ORDER BY gender_id;

-- purchaseテーブルとcustomerテーブルとgenderテーブルを結合
SELECT purchase.id AS '受注ID',customer.name AS '購入者氏名',gender.name AS '性別',product.name AS '商品名',product.price AS '価格',purchase.quantity AS '購入数',sum(product.price * purchase.quantity) AS '購入金額',color.name AS 'カラー',size.name AS 'サイズ',purchase.order_date AS '受注日'
FROM purchase
LEFT OUTER JOIN customer 
ON purchase.customer_id = customer.id
LEFT OUTER JOIN gender
ON customer.gender_id = gender.id
LEFT OUTER JOIN product_cs
ON purchase.product_cs_id = product_cs.id
LEFT OUTER JOIN product
ON product_cs.product_id = product.id
LEFT OUTER JOIN color
ON product_cs.color_id = color.id
LEFT OUTER JOIN size
ON product_cs.size_id = size.id
GROUP BY purchase.id,customer.name,gender.name,product.name,product.price,purchase.quantity,color.name,size.name,purchase.order_date
ORDER BY purchase.id,purchase.product_cs_id,gender_id,sum(product.price * purchase.quantity),color.id,size.id;




-- 課題 ---
SELECT customer.id AS '購入者', gender.name AS '性別',product.name AS '商品名',sum(purchase.quantity) AS '購入数',sum(product.price * purchase.quantity) AS '購入合計金額',purchase.order_date AS '受注日'
FROM purchase
LEFT OUTER JOIN customer 
ON purchase.customer_id = customer.id
LEFT OUTER JOIN gender
ON customer.gender_id = gender.id
LEFT OUTER JOIN product_cs
ON purchase.product_cs_id = product_cs.id
LEFT OUTER JOIN product
ON product_cs.product_id = product.id
LEFT OUTER JOIN color
ON product_cs.color_id = color.id
LEFT OUTER JOIN size
ON product_cs.size_id = size.id
GROUP BY gender.id,product.id,purchase.order_date
ORDER BY gender_id,product.id,sum(product.price * purchase.quantity);
/*
上記SQLを実行した結果
purchaseにindexを貼る必要有り
+----+-------------+------------+--------+---------------+---------+---------+----------------------------------+------+---------------------------------+
| id | select_type | table      | type   | possible_keys | key     | key_len | ref                              | rows | Extra                           |
+----+-------------+------------+--------+---------------+---------+---------+----------------------------------+------+---------------------------------+
|  1 | SIMPLE      | purchase   | ALL    | NULL          | NULL    | NULL    | NULL                             | 5000 | Using temporary; Using filesort |
|  1 | SIMPLE      | customer   | eq_ref | PRIMARY       | PRIMARY | 4       | db_kensyu.purchase.customer_id   |    1 | NULL                            |
|  1 | SIMPLE      | gender     | eq_ref | PRIMARY       | PRIMARY | 4       | db_kensyu.customer.gender_id     |    1 | NULL                            |
|  1 | SIMPLE      | product_cs | eq_ref | PRIMARY       | PRIMARY | 4       | db_kensyu.purchase.product_cs_id |    1 | NULL                            |
|  1 | SIMPLE      | product    | eq_ref | PRIMARY       | PRIMARY | 4       | db_kensyu.product_cs.product_id  |    1 | NULL                            |
|  1 | SIMPLE      | color      | eq_ref | PRIMARY       | PRIMARY | 4       | db_kensyu.product_cs.color_id    |    1 | Using index                     |
|  1 | SIMPLE      | size       | eq_ref | PRIMARY       | PRIMARY | 4       | db_kensyu.product_cs.size_id     |    1 | Using index                     |
+----+-------------+------------+--------+---------------+---------+---------+----------------------------------+------+---------------------------------+
*/

-- 性別ごとの購入数
SELECT gender.name AS '性別',sum(purchase.quantity) AS '購入数'
FROM purchase
LEFT OUTER JOIN customer
ON purchase.customer_id = customer.id
LEFT OUTER JOIN gender
ON customer.gender_id = gender.id
GROUP BY gender.name
ORDER BY gender_id;


-- 商品ごとの購入数
SELECT product.name AS '商品名',sum(purchase.quantity) AS '購入数'
FROM purchase
INNER JOIN product_cs
ON purchase.product_cs_id = product_cs.id
LEFT OUTER JOIN product
ON product_cs.product_id = product.id
GROUP BY product.name
ORDER BY product.id;

-- 受注日ごとの購入数
SELECT purchase.order_date AS '受注日',sum(purchase.quantity) AS '購入数'
FROM purchase
GROUP BY purchase.order_date
ORDER BY purchase.order_date;




--------- indexについて知るためにいろいろ実行 ------------

select *
from purchase
where customer_id = 40401;

/*
+----+-------------+----------+------+---------------------------------+---------------------------------+---------+-------+------+-------+
| id | select_type | table    | type | possible_keys                   | key                             | key_len | ref   | rows | Extra |
+----+-------------+----------+------+---------------------------------+---------------------------------+---------+-------+------+-------+
|  1 | SIMPLE      | purchase | ref  | index_customer_id_on_product_id | index_customer_id_on_product_id | 4       | const |    1 | NULL  |
+----+-------------+----------+------+---------------------------------+---------------------------------+---------+-------+------+-------+
*/

SELECT *
FROM purchase
LEFT OUTER JOIN customer 
ON purchase.customer_id = customer.id
where customer.id = 4040;

/*
+----+-------------+----------+-------+---------------------------------+---------------------------------+---------+-------+------+-------+
| id | select_type | table    | type  | possible_keys                   | key                             | key_len | ref   | rows | Extra |
+----+-------------+----------+-------+---------------------------------+---------------------------------+---------+-------+------+-------+
|  1 | SIMPLE      | customer | const | PRIMARY                         | PRIMARY                         | 4       | const |    1 | NULL  |
|  1 | SIMPLE      | purchase | ref   | index_customer_id_on_product_id | index_customer_id_on_product_id | 4       | const |    1 | NULL  |
+----+-------------+----------+-------+---------------------------------+---------------------------------+---------+-------+------+-------+
*/

SELECT purchase.id AS '受注ID',customer.name AS '購入者氏名',gender.name AS '性別',product.name AS '商品名',product.price AS '価格',color.name AS 'カラー',size.name AS 'サイズ'
FROM purchase
LEFT OUTER JOIN customer 
ON purchase.customer_id = customer.id
LEFT OUTER JOIN gender
ON customer.gender_id = gender.id
LEFT OUTER JOIN product_cs
ON purchase.product_cs_id = product_cs.id
LEFT OUTER JOIN product
ON product_cs.product_id = product.id
LEFT OUTER JOIN color
ON product_cs.color_id = color.id
LEFT OUTER JOIN size
ON product_cs.size_id = size.id
GROUP BY purchase.customer_id
having purchase.customer_id = 805
ORDER BY purchase.customer_id;

/*
+----+-------------+----------+--------+---------------------------------+---------------------------------+---------+--------------------------------+------+-------------+
| id | select_type | table    | type   | possible_keys                   | key                             | key_len | ref                            | rows | Extra       |
+----+-------------+----------+--------+---------------------------------+---------------------------------+---------+--------------------------------+------+-------------+
|  1 | SIMPLE      | purchase | index  | index_customer_id_on_product_id | index_customer_id_on_product_id | 8       | NULL                           | 5000 | Using index |
|  1 | SIMPLE      | customer | eq_ref | PRIMARY                         | PRIMARY                         | 4       | db_kensyu.purchase.customer_id |    1 | NULL        |
|  1 | SIMPLE      | gender   | eq_ref | PRIMARY                         | PRIMARY                         | 4       | db_kensyu.customer.gender_id   |    1 | NULL        |
+----+-------------+----------+--------+---------------------------------+---------------------------------+---------+--------------------------------+------+-------------+
*/

-- 複数会員を検索
select customer_id AS '会員番号',count(*) AS '重複数' 
from purchase
group by customer_id
having count(customer_id) > 1
and customer_id = 1926;

-- 会員の購入商品ごとの表
select purchase.id AS '受注ID',customer.name AS '購入者氏名',gender.name AS '性別',product.name AS '商品名',product.price AS '価格',purchase.quantity AS '購入数',(product.price * purchase.quantity) AS '購入金額',color.name AS 'カラー',size.name AS 'サイズ',purchase.order_date AS '受注日'
from purchase
LEFT OUTER JOIN customer 
ON purchase.customer_id = customer.id
LEFT OUTER JOIN gender
ON customer.gender_id = gender.id
LEFT OUTER JOIN product_cs
ON purchase.product_cs_id = product_cs.id
LEFT OUTER JOIN product
ON product_cs.product_id = product.id
LEFT OUTER JOIN color
ON product_cs.color_id = color.id
LEFT OUTER JOIN size
ON product_cs.size_id = size.id
where purchase.customer_id = 1926
ORDER BY purchase.customer_id;



SELECT purchase.customer_id AS '会員番号',customer.id AS '購入者氏名', gender.name AS '性別',purchase.product_cs_id AS '商品番号',product.name AS '商品名',sum(purchase.quantity) AS '購入数',sum(product.price * purchase.quantity) AS '購入合計金額',purchase.order_date AS '受注日'
FROM purchase
LEFT OUTER JOIN customer 
ON purchase.customer_id = customer.id
LEFT OUTER JOIN gender
ON customer.gender_id = gender.id
LEFT OUTER JOIN product_cs
ON purchase.product_cs_id = product_cs.id
LEFT OUTER JOIN product
ON product_cs.product_id = product.id
LEFT OUTER JOIN color
ON product_cs.color_id = color.id
LEFT OUTER JOIN size
ON product_cs.size_id = size.id
GROUP BY purchase.id,customer.id, gender.name,purchase.product_cs_id,product.name,purchase.order_date
limit 10;


select
purchase.id AS '受注ID'
,purchase.customer_id AS '会員番号'
,customer.name AS '購入者氏名'
,gender.name AS '性別'
,product.name AS '商品名'
,product.price AS '価格'
,purchase.quantity AS '購入数'
,(product.price * purchase.quantity) AS '購入金額'
,color.name AS 'カラー'
,size.name AS 'サイズ'
,purchase.order_date AS '受注日'
from purchase
left outer join customer
on purchase.customer_id = customer.id
left outer join gender
on customer.gender_id = gender.id
left outer join product_cs
on purchase.product_cs_id = product_cs.id
left outer join product
on product_cs.product_id = product.id
left outer join color
on product_cs.color_id = color.id
left outer join size
on product_cs.size_id = size.id
where purchase.customer_id = 1926
group by
purchase.id
,purchase.customer_id
,customer.name
,gender.name
having purchase.id
order by purchase.id;
/*
上記実行結果

+----+-------------+------------+--------+---------------------------------------------+---------------------------------+---------+----------------------------------+------+---------------------------------+
| id | select_type | table      | type   | possible_keys                               | key                             | key_len | ref                              | rows | Extra                           |
+----+-------------+------------+--------+---------------------------------------------+---------------------------------+---------+----------------------------------+------+---------------------------------+
|  1 | SIMPLE      | purchase   | ref    | index_customer_id_on_product_id,customer_id | index_customer_id_on_product_id | 4       | const                            |    3 | Using temporary; Using filesort |
|  1 | SIMPLE      | customer   | const  | PRIMARY                                     | PRIMARY                         | 4       | const                            |    1 | NULL                            |
|  1 | SIMPLE      | gender     | eq_ref | PRIMARY                                     | PRIMARY                         | 4       | db_kensyu.customer.gender_id     |    1 | NULL                            |
|  1 | SIMPLE      | product_cs | eq_ref | PRIMARY                                     | PRIMARY                         | 4       | db_kensyu.purchase.product_cs_id |    1 | NULL                            |
|  1 | SIMPLE      | product    | eq_ref | PRIMARY                                     | PRIMARY                         | 4       | db_kensyu.product_cs.product_id  |    1 | NULL                            |
|  1 | SIMPLE      | color      | eq_ref | PRIMARY                                     | PRIMARY                         | 4       | db_kensyu.product_cs.color_id    |    1 | NULL                            |
|  1 | SIMPLE      | size       | eq_ref | PRIMARY                                     | PRIMARY                         | 4       | db_kensyu.product_cs.size_id     |    1 | NULL                            |
+----+-------------+------------+--------+---------------------------------------------+---------------------------------+---------+----------------------------------+------+---------------------------------+
*/