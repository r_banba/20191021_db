-- 終端文字を変更
DELIMITER //
-- 処理が終了したらプロシージャを削除する
DROP PROCEDURE IF EXISTS update_data;

CREATE PROCEDURE update_data()
BEGIN
    DECLARE cnt INT default(0);
    -- exception,warningが出たら終了させる
    DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
    BEGIN
        -- mysqlのエラーコードを取得
        GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
        -- エラーがあった場合に、エラー内容をselect文で表示する
        SELECT @sqlstate, @errno, @text;
        ROLLBACK;
    END;

    データ投入処理
    START TRANSACTION;
        -- 投入するデータの数
        SET @i = 1;
        
        -- productテーブルの価格の更新
        WHILE @i <= 50 DO
            SET @price = FLOOR(500+RAND() * 9501);
            UPdATE product set price = @price where id = @i;
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;

        SET @i = 1;

        -- purchaseテーブルの受注日の更新
        WHILE @i <= 50 DO
            SET @price = FLOOR(500+RAND() * 9501);
            UPdATE product set price = @price where id = @i;
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;

        SET @i = 1;
        -- purchaseテーブルの受注日の更新
        WHILE @i <= 50 DO
            SET @date = DATE_FORMAT(now(), '%y-%m-%d');
            IF @i%2 = 0 THEN
            UPDATE purchase set order_date = @date where id = @i;
            END IF;
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;


        SET @i = 401;
        データの追加
        WHILE @i <= 5000 DO
            -- 1から3までの乱数（性別）を生成
            SET @gender = FLOOR(1+RAND() * 3);
            -- productテーブルにデータを登録（親テーブル）
            INSERT INTO product(name,price) VALUES ( concat('商品',@i), 1000 + @i );
            -- customerテーブルにデータを登録（子テーブル）
            INSERT INTO customer(name,barthday,gender_id) VALUES ( concat('テスト氏名',@i), DATE_FORMAT(now(), '%y-%m-%d'), @gender );
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;
        select * from product;
        
        SET @i = 51;

        WHILE @i <= 5000 DO
            -- 1から5000までの乱数（商品ID）を生成
            SET @product = FLOOR(51+RAND() * 4949);
            -- 1から5までの乱数（色）を生成
            SET @color = FLOOR(1+RAND() * 5);
            -- 1から4までの乱数（サイズ）を生成
            SET @size = FLOOR(1+RAND() * 4);

            -- 実行結果の有無で重複排除
            SET @count = 0;
            SELECT count(*) INTO @count FROM product_cs WHERE product_id = @product AND color_id = @color AND size_id = @size;
            IF @count = 0 THEN
            -- product_csテーブルにデータを登録（子テーブル）
            INSERT INTO product_cs(product_id,color_id,size_id) VALUES( @product, @color, @size);
            SELECT @i + 1 INTO @num;
            SET @i = @num;
            END IF;
        END WHILE;

        SET @i = 51;

        WHILE @i <= 5000 DO
            SET @customer = FLOOR(51+RAND() * 4949);
            SET @product_cs = FLOOR(51+RAND() * 4949);
            SET @quantity = FLOOR(1+RAND() * 10);
            INSERT INTO purchase(customer_id,product_cs_id,order_date,quantity) VALUES( @customer, @product_cs,DATE_FORMAT(now(), '%y-%m-%d'),@quantity);
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;

    COMMIT;
END
//
DELIMITER ;

call update_data();