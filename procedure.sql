-- 終端文字を変更
DELIMITER //
-- 処理が終了したらプロシージャを削除する
DROP PROCEDURE IF EXISTS insert_data;

CREATE PROCEDURE insert_data()
BEGIN
    DECLARE cnt INT default(0);
    -- exception,warningが出たら終了させる
    DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
    BEGIN
        -- mysqlのエラーコードを取得
        GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
        -- エラーがあった場合に、エラー内容をselect文で表示する
        SELECT @sqlstate, @errno, @text;
        ROLLBACK;
    END;

    -- データ投入処理
    START TRANSACTION;
        -- 投入するデータの数
        SET @i = 1;
        
        -- genderテーブルにデータを登録（親テーブル）
        INSERT INTO gender(name) VALUES ('男'),('女'),('その他');

        -- colorテーブルにデータを登録（親テーブル）
        INSERT INTO color(name) VALUES ('赤'),('青'),('黃'),('黒'),('白');

        -- sizeテーブルにデータを登録（親テーブル）
        INSERT INTO size(name) VALUES ('S'),('M'),('L'),('XL');

        WHILE @i <= 50 DO
            -- 1から3までの乱数（性別）を生成
            SET @gender = FLOOR(1+RAND() * 3);
            -- productテーブルにデータを登録（親テーブル）
            INSERT INTO product(name,price) VALUES ( concat('商品',@i), 1000 + @i );
            -- customerテーブルにデータを登録（子テーブル）
            INSERT INTO customer(name,barthday,gender_id) VALUES ( concat('テスト氏名',@i), DATE_FORMAT(now(), '%y-%m-%d'), @gender );
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;

        SET @i = 1;

        WHILE @i <= 50 DO
            -- 1から50までの乱数（商品ID）を生成
            SET @product = FLOOR(1+RAND() * 50);
            -- 1から5までの乱数（色）を生成
            SET @color = FLOOR(1+RAND() * 5);
            -- 1から4までの乱数（サイズ）を生成
            SET @size = FLOOR(1+RAND() * 4);
            -- product_csテーブルにデータを登録（子テーブル）
            INSERT INTO product_cs(product_id,color_id,size_id) VALUES( @product, @color, @size);
            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;

        WHILE @i <= 50 DO
            -- 1から50までの乱数（会員ID）を生成
            SET @customer = FLOOR(1+RAND() * 50);
            -- 1から50までの乱数（商品ID）を生成
            SET @product = FLOOR(1+RAND() * 50);
            -- 1から10までの乱数（数量）を生成
            SET @num = FLOOR(1+RAND() * 10);

            -- purchaseテーブルにデータを登録（子テーブル）
            INSERT INTO purchase(customer_id,product_cs_id,order_date,quantity) VALUES( @customer, @product, DATE_FORMAT(now(), '%y-%m-%d'), @num);

            SELECT @i + 1 INTO @num;
            SET @i = @num;
        END WHILE;
    COMMIT;
END
//
DELIMITER ;

call insert_data();